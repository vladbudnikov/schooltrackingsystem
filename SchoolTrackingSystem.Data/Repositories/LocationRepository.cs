﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolTrackingSystem.Data.Infrastructure;
using SchoolTrackingSystem.Models;

namespace SchoolTrackingSystem.Data.Repositories
{
   public class LocationRepository: RepositoryBase<Location>, ILocationRepository
    {
        public LocationRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public Location GetLocationByName(string name)
        {
            return this.DbContext.Locations.FirstOrDefault(l => l.Name == name);
        }
    }

    public interface ILocationRepository:IRepository<Location>
    {
        Location GetLocationByName(string name);
    }
}
