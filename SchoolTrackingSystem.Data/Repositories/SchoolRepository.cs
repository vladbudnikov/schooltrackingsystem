﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolTrackingSystem.Data.Infrastructure;
using SchoolTrackingSystem.Models;

namespace SchoolTrackingSystem.Data.Repositories
{
    public class SchoolRepository : RepositoryBase<School>, ISchoolRepository
    {
        public SchoolRepository(IDbFactory dbFactory)
            : base(dbFactory) { }

        public School GetSchoolByName(string schoolName)
        {
            var school = this.DbContext.Schools.FirstOrDefault(s => s.Name == schoolName);
            return school;
        }
    }

    public interface ISchoolRepository : IRepository<School>
    {
        School GetSchoolByName(string schoolName);
    }
}
