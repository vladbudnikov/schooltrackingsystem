﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolTrackingSystem.Data.Infrastructure;
using SchoolTrackingSystem.Models;

namespace SchoolTrackingSystem.Data.Repositories
{
    public class TeacherRepository : RepositoryBase<Teacher>, ITeacherRepository
    {
        public TeacherRepository(IDbFactory dbFactory)
            : base(dbFactory) { }

        public Teacher GetTeacherByLastName(string teacherLastName)
        {
            var teacher = this.DbContext.Teachers.FirstOrDefault(c => c.LastName == teacherLastName);

            return teacher;
        }

        public override void Update(Teacher entity)
        {
            entity.DateUpdated = DateTime.Now;
            base.Update(entity);
        }
    }

    public interface ITeacherRepository : IRepository<Teacher>
    {
        Teacher GetTeacherByLastName(string teacherLastName);
    }
}
