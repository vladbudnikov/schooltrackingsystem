using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using SchoolTrackingSystem.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace SchoolTrackingSystem.Data.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            //context.Locations.AddOrUpdate(
            //    l => l.Name,
            //    new Location { Name = "Sofia" },
            //    new Location { Name = "Vratza" },
            //    new Location { Name = "Veliko Tyrnovo" });
            //context.SaveChanges();
            //context.Schools.AddOrUpdate(
            //    s => s.Name,
            //    new School
            //    {
            //        Name = "SOU J. Baucher",
            //        LocationId = context.Locations.FirstOrDefault(l => l.Name == "Sofia").Id
            //    },
            //    new School
            //    {
            //        Name = "SOU J. Joyce",
            //        LocationId = context.Locations.FirstOrDefault(l => l.Name == "Sofia").Id
            //    },
            //    new School
            //    {
            //        Name = "SOU L. Karavelov",
            //        LocationId = context.Locations.FirstOrDefault(l => l.Name == "Sofia").Id
            //    },
            //    new School
            //    {
            //        Name = "SOU S. Vrachanski",
            //        LocationId = context.Locations.FirstOrDefault(l => l.Name == "Vratza").Id
            //    },
            //    new School
            //    {
            //        Name = "SOU Patriarh Evtimi",
            //        LocationId = context.Locations.FirstOrDefault(l => l.Name == "Veliko Tyrnovo").Id
            //    });
            //context.SaveChanges();

            //context.Subjects.AddOrUpdate(
            //    x => x.Name,
            //    new Subject { Name = "History" },
            //    new Subject { Name = "Literature" },
            //    new Subject { Name = "Language" },
            //    new Subject { Name = "Math" }
            //    );
            //context.SaveChanges();

            //if (!context.Forms.Any())
            //{
            //    var forms = new HashSet<Form>();
            //    var indexes = new List<string> { "a", "b", "c", "d" };
            //    foreach (var school in context.Schools)
            //    {
            //        for (var i = 1; i < 13; i++)
            //        {
            //            foreach (var index in indexes)
            //            {
            //                forms.Add(new Form { Level = i, Index = index, SchoolId = school.Id });
            //            }
            //        }
            //    }
            //    context.Forms.AddRange(forms);
            //    context.SaveChanges();
            //}
            //try
            //{
            //    var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            //    roleManager.Create(new IdentityRole("Admin"));
            //    roleManager.Create(new IdentityRole("User"));
            //    context.SaveChanges();
            //}
            //catch (Exception ex )
            //{

            //    throw ex;
            //}

            //try
            //{
            //    var school = context.Schools.FirstOrDefault(s=>s.Name = SchoolName);
            //    var teacher = new Teacher
            //    {
            //        UserName = "k_user",
            //        Email = "k@a.b",
            //        FirstName = "Kiril",
            //        MiddleName = "Draganov",
            //        LastName = "Gocev",
            //        School = school
            //    };

            //    string password = "secret";

            //    using (context)
            //    {
            //        var store = new UserStore<Teacher>(context);
            //        var manager = new UserManager<Teacher, string>(store);
            //        IdentityResult result;
            //        try
            //        {

            //            result = manager.Create(teacher, password);
            //        }
            //        catch (Exception ex)
            //        {

            //            throw;
            //        }
            //        if (!result.Succeeded)
            //            throw new ApplicationException("Unable to create a user.");

            //        result = manager.AddToRole(teacher.Id, "User");
            //        if (!result.Succeeded)
            //            throw new ApplicationException("Unable to add user to a role.");
            //        context.Teachers.Add(teacher);
            //        context.SaveChanges();
            //    }

            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e.Message);
            //}


            //    //    if (!context.Teachers.Any())
            //    //    {
            //    //        var r = new Random();
            //    //        var schoolIds = context.Schools.Select(x => x.Id).ToList();

            //    //        var teachers = new List<Teacher>
            //    //        {
            //    //            new Teacher
            //    //            {
            //    //                FirstName = "Ivan",
            //    //                MiddleName = "Ivanov",
            //    //                LastName = "Ovcharski",
            //    //                SchoolId = r.Next(schoolIds[0], schoolIds.Count() - 1)
            //    //            },
            //    //            new Teacher
            //    //            {
            //    //                FirstName = "Maria",
            //    //                MiddleName = "Ivanova",
            //    //                LastName = "Daskalska",
            //    //                SchoolId = r.Next(schoolIds[0], schoolIds.Count() - 1)
            //    //            },
            //    //            new Teacher
            //    //            {
            //    //                FirstName = "Elena",
            //    //                MiddleName = "Petrova",
            //    //                LastName = "Yordanova",
            //    //                SchoolId = r.Next(schoolIds[0], schoolIds.Count() - 1)
            //    //            },
            //    //            new Teacher
            //    //            {
            //    //                FirstName = "Galya",
            //    //                MiddleName = "Stoyanova",
            //    //                LastName = "Vitanova",
            //    //                SchoolId = r.Next(schoolIds[0], schoolIds.Count() - 1)
            //    //            },
            //    //            new Teacher
            //    //            {
            //    //                FirstName = "Diana",
            //    //                MiddleName = "Rudolf",
            //    //                LastName = "Shtain",
            //    //                SchoolId = r.Next(schoolIds[0], schoolIds.Count() - 1)
            //    //            },
            //    //            new Teacher
            //    //            {
            //    //                FirstName = "Dimityr",
            //    //                MiddleName = "Genadiev",
            //    //                LastName = "Oresharski",
            //    //                SchoolId = r.Next(schoolIds[0], schoolIds.Count() - 1)
            //    //            },
            //    //            new Teacher
            //    //            {
            //    //                FirstName = "Irina",
            //    //                MiddleName = "Zdravkova",
            //    //                LastName = "Zdravkova",
            //    //                SchoolId = r.Next(schoolIds[0], schoolIds.Count() - 1)
            //    //            }
            //    //        };
            //    //        context.Teachers.AddRange(teachers);
            //    //        context.SaveChanges();
            //    //    }
        }

    }
    }