﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Cryptography;

namespace SchoolTrackingSystem.Models
{
    [Table("Student")]
    public class Student:User
    {
        [Required]
        [MinLength(1)]
        [MaxLength(200)]
        public string FirstName { get; set; }
        [Required]
        [MinLength(1)]
        [MaxLength(200)]
        public string MiddleName { get; set; }
        [Required]
        [MinLength(1)]
        [MaxLength(200)]
        public string LastName { get; set; }
        public int? FormId { get; set; }
        public Form Form { get; set; }
        public virtual ICollection<Subject> Subjects { get; set; }
    }
}