﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolTrackingSystem.Models
{
    [Table("Teacher")]
    public class Teacher:User
    {
        public Teacher()
        {
            DateCreated = DateTime.Now;
        }
        [Required]
        [MinLength(1)]
        [MaxLength(200)]
        public string FirstName { get; set; }
        [Required]
        [MinLength(1)]
        [MaxLength(200)]
        public string MiddleName { get; set; }
        [Required]
        [MinLength(1)]
        [MaxLength(200)]
        public string LastName { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int SchoolId { get; set; }
        public virtual School School { get; set; }
        public virtual ICollection<Form> Forms { get; set; }
        public virtual ICollection<Subject> Subjects { get; set; }
    }
}