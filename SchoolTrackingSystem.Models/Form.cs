﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SchoolTrackingSystem.Models
{
    public class Form
    {
        public int Id { get; set; }

        [Range(1, 12)]
        public int Level { get; set; }

        public string Index { get; set; }

        public string Name => this.Level + this.Index; //eg, 4-b,5-a
        public int SchoolId { get; set; }
        public virtual School School { get; set; }
        public virtual ICollection<Homework> Homeworks { get; set; }
        public virtual ICollection<Student> Students { get; set; }
        public virtual ICollection<Teacher> Teachers { get; set; }
        public virtual ICollection<Subject> Subjects { get; set; }
    }
}