﻿using System.Collections.Generic;

namespace SchoolTrackingSystem.Models
{
    public class Homework
    {
        public int Id { get; set; }
        public int SubjectId { get; set; }
        public Subject Subject { get; set; }
        public int FormId { get; set; }
        public Form Form { get; set; }
        public virtual ICollection<Form> Forms { get; set; }
    }
}