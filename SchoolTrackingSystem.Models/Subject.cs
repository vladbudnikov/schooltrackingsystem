﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

namespace SchoolTrackingSystem.Models
{
    public class Subject
    {
        public int Id { get; set; }
        [Index(IsUnique = true)]
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        public virtual ICollection<Teacher> Teachers { get; set; }
        public virtual ICollection<Student> Students { get; set; }
        public virtual ICollection<Form> Forms { get; set; }
        public virtual ICollection<School> Schools { get; set; }
    }
}