﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SchoolTrackingSystem.Models
{
    public class School
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int LocationId { get; set; }

        public virtual Location Location { get; set; }
        public virtual ICollection<Form> Forms { get; set; }
        public virtual ICollection<Subject> Subjects { get; set; }
    }
}