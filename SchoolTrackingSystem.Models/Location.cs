﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolTrackingSystem.Models
{
    public class Location
    {
        public int Id { get; set; }
        [Index(IsUnique = true)]
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
    }
}