﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolTrackingSystem.Data.Infrastructure;
using SchoolTrackingSystem.Data.Repositories;
using SchoolTrackingSystem.Models;

namespace SchoolTrackingSystem.Service
{
    // operations to be exposed

    public class TeacherService : ITeacherService
    {
        private readonly ITeacherRepository teachersRepository;
        private readonly IUnitOfWork unitOfWork;

        public TeacherService(ITeacherRepository teachersRepository, IUnitOfWork unitOfWork)
        {
            this.teachersRepository = teachersRepository;
            this.unitOfWork = unitOfWork;
        }

        #region ITeacherService Members

        public IEnumerable<Teacher> GetTeachers()
        {
            var teachers = teachersRepository.GetAll();
            return teachers;
        }

        public Teacher GetTeacher(int id)
        {
            var teacher = teachersRepository.GetById(id);
            return teacher;
        }

        public void CreateTeacher(Teacher teacher)
        {
            teachersRepository.Add(teacher);
        }

        public void SaveTeacher()
        {
            unitOfWork.Commit();
        }

        #endregion

    }
    public interface ITeacherService
    {
        IEnumerable<Teacher> GetTeachers();
        Teacher GetTeacher(int id);
        void CreateTeacher(Teacher teacher);
        void SaveTeacher();
    }
}
