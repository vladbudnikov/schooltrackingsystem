﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolTrackingSystem.Data.Infrastructure;
using SchoolTrackingSystem.Data.Repositories;
using SchoolTrackingSystem.Models;

namespace SchoolTrackingSystem.Service
{
    // operations want to be exposed


    public class SchoolService : ISchoolService
    {
        private readonly ISchoolRepository _schoolsRepository;
        private readonly IUnitOfWork _unitOfWork;

        public SchoolService(ISchoolRepository schoolsRepository, IUnitOfWork unitOfWork)
        {
            this._schoolsRepository = schoolsRepository;
            this._unitOfWork = unitOfWork;
        }

        #region ISchoolService Members

        public IEnumerable<School> GetSchools(string name = null)
        {
            if (string.IsNullOrEmpty(name))
                return _schoolsRepository.GetAll();
            else
                return _schoolsRepository.GetAll().Where(c => c.Name == name);
        }

        public School GetSchool(int id)
        {
            var school = _schoolsRepository.GetById(id);
            return school;
        }

        public School GetSchool(string name)
        {
            var school = _schoolsRepository.GetSchoolByName(name);
            return school;
        }

        public void CreateSchool(School school)
        {
            _schoolsRepository.Add(school);
        }

        public void SaveSchool()
        {
            _unitOfWork.Commit();
        }

        #endregion
    }
    public interface ISchoolService
    {
        IEnumerable<School> GetSchools(string name = null);
        School GetSchool(int id);
        School GetSchool(string name);
        void CreateSchool(School school);
        void SaveSchool();
    }
}