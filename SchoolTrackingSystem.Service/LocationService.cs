﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolTrackingSystem.Data.Infrastructure;
using SchoolTrackingSystem.Data.Repositories;
using SchoolTrackingSystem.Models;

namespace SchoolTrackingSystem.Service
{
    public class LocationService : ILocationService
    {
        private readonly ILocationRepository _locationsRepository;
        private readonly IUnitOfWork _unitOfWork;

        public LocationService(ILocationRepository locationsRepository, IUnitOfWork unitOfWork)
        {
            this._locationsRepository = locationsRepository;
            this._unitOfWork = unitOfWork;
        }

        #region ISchoolService Members

        public IEnumerable<Location> GetLocations(string name = null)
        {
            if (string.IsNullOrEmpty(name))
                return _locationsRepository.GetAll();
            else
                return _locationsRepository.GetAll().Where(c => c.Name == name);
        }

        public Location GetLocation(int id)
        {
            var location = _locationsRepository.GetById(id);
            return location;
        }

        public Location GetLocation(string name)
        {
            var location = _locationsRepository.GetLocationByName(name);
            return location;
        }

        public void CreateLocation(Location location)
        {
            _locationsRepository.Add(location);
        }

        public void SaveLocation()
        {
            _unitOfWork.Commit();
        }

        #endregion
    }
    public interface ILocationService
    {
        IEnumerable<Location> GetLocations(string name = null);
        Location GetLocation(int id);
        Location GetLocation(string name);
        void CreateLocation(Location location);
        void SaveLocation();
    }
}

