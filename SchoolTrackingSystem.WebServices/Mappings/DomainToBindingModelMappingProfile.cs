﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using SchoolTrackingSystem.Models;
using SchoolTrackingSystem.WebServices.BindingModels;

namespace SchoolTrackingSystem.WebServices.Mappings
{
    public class DomainToBindingModelMappingProfile : Profile
    {
        public DomainToBindingModelMappingProfile()
        {
            CreateMap<School, SchoolBindingModel>();
            //CreateMap<Teacher, TeacherBindingModel>();
        }
        public override string ProfileName
        {
            get
            {
                return "DomainToBindingModelMapping";
            }
        }
    }
}


