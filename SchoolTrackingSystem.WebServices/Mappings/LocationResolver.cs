﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using SchoolTrackingSystem.Data;
using SchoolTrackingSystem.Data.Infrastructure;
using SchoolTrackingSystem.Models;
using SchoolTrackingSystem.Service;
using SchoolTrackingSystem.WebServices.BindingModels;

namespace SchoolTrackingSystem.WebServices.Mappings
{
    public class LocationResolver : IValueResolver<SchoolBindingModel, School, Location>
    {

        private readonly ILocationService _locationService;

        public LocationResolver(ILocationService locationService)
        {
            this._locationService = locationService;
        }

        public Location Resolve(SchoolBindingModel source, School destination, Location destMember, ResolutionContext context)
        {
            return _locationService.GetLocation((source.LocationName));
        }
    }
}