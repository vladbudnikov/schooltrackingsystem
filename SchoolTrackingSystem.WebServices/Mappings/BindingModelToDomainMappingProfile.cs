﻿using AutoMapper;
using SchoolTrackingSystem.Models;
using SchoolTrackingSystem.WebServices.BindingModels;

namespace SchoolTrackingSystem.WebServices.Mappings
{
    public class BindingModelToDomainMappingProfile:Profile
    {
        public BindingModelToDomainMappingProfile()
        {
            CreateMap<SchoolBindingModel, School>()
                .ForMember(dest => dest.Name, map => map.MapFrom(src => src.Name))
                .ForMember(dest => dest.Location, opt => opt.ResolveUsing<LocationResolver>());
            //Mapper.AssertConfigurationIsValid();
            //var source = new SchoolBindingModel
            //{
            //    Name = "SOU",
            //    LocationName = "Sofia"
            //};

            //var result = Mapper.Map<SchoolBindingModel, School>(source);

            //result.Location.Id.Equals(122343);
        }
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }
    }
}