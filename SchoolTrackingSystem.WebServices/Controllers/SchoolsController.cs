﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using SchoolTrackingSystem.Models;
using SchoolTrackingSystem.WebServices.BindingModels;
using SchoolTrackingSystem.Service;

namespace SchoolTrackingSystem.WebServices.Controllers
{
    public class SchoolsController : ApiController
    {
        private readonly ISchoolService _schoolService;

        public SchoolsController(ISchoolService schoolService)
        {
            this._schoolService = schoolService;
        }

        // GET: api/Teachers
        public IEnumerable<SchoolBindingModel> GetSchools([FromUri]SchoolBindingModel school)
        {
            var schools = _schoolService.GetSchools(school.Name);
            var schoolBindingModel = Mapper.Map<IEnumerable<School>, IEnumerable<SchoolBindingModel>>(schools);
            return schoolBindingModel;
        }

        // POST: api/PostSchool
        [ResponseType(typeof(SchoolBindingModel))]
        public IHttpActionResult PostSchool(SchoolBindingModel schoolCandidate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var school = Mapper.Map<SchoolBindingModel, School>(schoolCandidate);
            _schoolService.CreateSchool(school);

            //try
            //{
            //    db.SaveChanges();
            //}
            //catch (DbUpdateException ex)
            //{
            //    if (TeacherExists(teacher.Id))
            //    {
            //        return Conflict();
            //    }
            //    else
            //    {
            //        throw ex;
            //    }
            //}

            return CreatedAtRoute("DefaultApi", new { id = school.Id }, schoolCandidate);
        }
    }
}