﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using SchoolTrackingSystem.Models;

namespace SchoolTrackingSystem.WebServices.BindingModels
{
    public class SchoolBindingModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string LocationName { get; set; }
    }
}